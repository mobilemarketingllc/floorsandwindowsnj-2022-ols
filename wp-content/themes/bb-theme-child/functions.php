<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );


add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);   
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);    
});

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.@$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );



function my_bb_custom_fonts ( $system_fonts ) {

  $system_fonts[ 'Vela Sans ExtLt' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '200',
    ),
  );

  $system_fonts[ 'Vela Sans' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '400',
    ),
  );
  
  $system_fonts[ 'Vela Sans Med' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '500',
    ),
  );	
 	
  $system_fonts[ 'Vela Sans Bd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '700',
    ),
  );	
	
   $system_fonts[ 'Vela Sans ExtBd' ] = array(
    'fallback' => 'Open Sans, sans-serif',
    'weights' => array(
      '900',
    ),
  );	



    return $system_fonts;
}

//Add to Beaver Builder Theme Customizer
add_filter( 'fl_theme_system_fonts', 'my_bb_custom_fonts' );

//Add to Beaver Builder modules
add_filter( 'fl_builder_font_families_system', 'my_bb_custom_fonts' );






// Product landing page product list

function product_category_products_function_for_specific_product($atts){

    $args = array(
        "post_type" => $atts[0],
        "post_status" => "publish",
        'meta_query' => array(
            array(
              'key' => 'sku',
              'value' => array('CRTNSBLLMSTN_L343_12X24_RL','CMPSTN_CP01_10X14_RG','RTQTY_AQ01_12X24_RM','BRZLBLCKSLT_S762_12X12_SN'),
              'compare' => 'IN'
              )      
        )
     );

      $the_query = new WP_Query( $args );
    
    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper landing_page_wrapper">				
				<div class="row product-row">';				
								
              
                while ($the_query->have_posts()) {
                $the_query->the_post();
                $image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
                $style = "padding: 5px;";
       
    $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="'.get_the_permalink().'" title="'.get_the_title().'">
                    <img class="list-pro-image" src="'.$image.'" alt="'.get_the_title().'">';
                          
                          if( get_field('quick_ship') == 1){ 

                            $content .='<div class="bb_special"><p>QUICK SHIP</p></div>';

                                } 

                       $content .='</a>'.brandroomvo_script_integration(get_field('manufacturer') ,get_field('sku') ,get_the_ID()).'</div>';

    $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="'.get_the_permalink().'" title="'.get_the_title().'" class="collection_text">'.get_field('collection').'</a> 
                            <a href="'.get_the_permalink().'" title="'.get_the_title().'"><span class="color_text"> 
                            '.get_field('color').' </span></a>
                                <span class="brand_text">'.get_field('brand').' </span> </h4>
                        </div>            
                </div>
            </div>';
           
    } 
       
 wp_reset_postdata(); 
						
   $content .= '</div>
			</div>';
            
            return $content;

} 

// Diff collection products

add_shortcode( 'product_listing_tile', 'product_category_products_function_for_specific_product' );



